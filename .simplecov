SimpleCov.start 'rails' do
  add_filter 'lib/generators'
  coverage_dir 'coverage/rspec'
end
