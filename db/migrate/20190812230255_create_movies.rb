class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.text :description, null: false, default: ''

      t.timestamps
    end
  end
end
